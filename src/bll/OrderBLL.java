package bll;

import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.OrderDAO;
import model.Order;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class OrderBLL {
    private List<Validator<Order>> validators;
    private OrderDAO orderDAO;

    public OrderBLL(){
        validators= new ArrayList<Validator<Order>>();
        validators.add(new QuantityValidator());

        orderDAO = new OrderDAO();

    }
    public Order findOrderById(int id) {
        Order st = OrderDAO.findByIdOrder(id);
        if (st == null) {
            throw new NoSuchElementException("The order with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertOrder(Order order) {
        for (Validator<Order> v : validators) {
            v.validate(order);
        }
        return OrderDAO.insertOrder(order);
    }
    public void deleteOrdr(String id) {

        OrderDAO.deleteOrder(id);
    }
    public void updateOrder(Order order) {

        for (Validator<Order> v : validators) {
            v.validate(order);
        }
        OrderDAO.updateOrder(order);
    }

}
