package bll;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.PriceValidator;
import dao.ProductDAO;
import model.Client;

import bll.validators.Validator;
import dao.ClientDAO;
import model.Product;

public class ProductBLL {
    private List<Validator<Product>> validators;
    private ProductDAO productDAO;


    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new PriceValidator());
        productDAO=new ProductDAO();
    }

    public Product findProductById(int id) {
        Product st = ProductDAO.findByIdProduct(id);
        if (st == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertProduct(Product product) {
        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        return ProductDAO.insertProduct(product);
    }
    public void deleteProduct(String id) {

        ProductDAO.deleteProduct(id);
    }
    public void updateProduct(Product product, String id) {

        for (Validator<Product> v : validators) {
            v.validate(product);
        }
        ProductDAO.updateProduct(product,id);
    }

    public void updateQ(int q, String id){
        ProductDAO.updateQ(q,id);
    }

}
