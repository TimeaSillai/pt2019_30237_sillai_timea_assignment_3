package bll.validators;

import model.Product;

public class PriceValidator implements Validator<Product> {

    private static final int MIN_PRICE = 1;
    private static final int MAX_PRICE = 30;

    @Override
    public void validate(Product t) {

        if (t.getPriceProduct() < MIN_PRICE || t.getPriceProduct() > MAX_PRICE) {
            throw new IllegalArgumentException("The Product price limit is not respected!");
        }

    }

}

