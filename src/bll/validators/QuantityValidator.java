package bll.validators;

import model.Order;

public class QuantityValidator implements Validator<Order> {

    private static final int MIN_Q=1;
    private static final int MAX_Q=40;


    @Override
    public void validate(Order order) {
        if (order.getQuantityOrder() < MIN_Q || order.getQuantityOrder() > MAX_Q) {
            throw new IllegalArgumentException("The Order quantity limit is not respected!");
        }
    }
}
