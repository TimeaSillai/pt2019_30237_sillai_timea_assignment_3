package dao;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;

public class ClientDAO {

	protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO client (idClient,nameClient,addressClient, email)"
			+ " VALUES (?,?,?,?)";
	private final static String findStatementString = "SELECT * FROM client where idClient = ?";
	public static Client findById(int idClient) {
		Client toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, idClient);
			rs = findStatement.executeQuery();
			rs.next();

			String name = rs.getString("nameClient");
			String address = rs.getString("addressClient");
			String email = rs.getString("email");

			toReturn = new Client(idClient, name, address, email);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int insert(Client client) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = 0;
		ResultSet rs=null;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			rs=insertStatement.getGeneratedKeys();
			if(rs.next()) {
				insertedId = rs.getInt(1);
			}
				insertStatement.setLong(1,insertedId);
				insertStatement.setString(2, client.getNameClient());
				insertStatement.setString(3, client.getAddressClient());
				insertStatement.setString(4, client.getEmail());
				insertStatement.executeUpdate();


		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
		} finally {
			try{
				if(rs != null) rs.close();
			} catch(Exception ex){}
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	public static void delete(String idClient) {
		Connection dbConnection = ConnectionFactory.getConnection();
		Statement s = null;
		try {
			s = dbConnection.createStatement();

			String sql = "DELETE FROM client  WHERE " +
					"idClient = '" + idClient + "' ";
			s.execute(sql);

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(dbConnection);
		}
	}
	public static void update(Client client, String id) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement preparedStmt=null;

		int updatedId = -1;
		try {
			String q="UPDATE client SET nameClient=?,addressClient=? ,email=? WHERE " + "idClient= '" + id + "' ";
			preparedStmt = dbConnection.prepareStatement(q);
			preparedStmt.setString(1, client.getNameClient());
			preparedStmt.setString(2, client.getAddressClient());
			preparedStmt.setString(3,client.getEmail());
			//preparedStmt.setLong(4, client.getIdClient());
			preparedStmt.executeUpdate();

		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ClientDAO:update " + e.getMessage());
		} finally {
			ConnectionFactory.close(preparedStmt);
			ConnectionFactory.close(dbConnection);
		}
		//return updatedId;

	}
}