package dao;

import connection.ConnectionFactory;
import model.Client;
import model.Order;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderDAO {

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO `order` (idOrder,quantityOrder,price,name)"
            + " VALUES (?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM order where idOrder = ?";
    public static Order findByIdOrder(int id) {
        Order toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, id);
            rs = findStatement.executeQuery();
            rs.next();

            int quantity = rs.getInt("quantityOrder");
            int price = rs.getInt("price");
            String name = rs.getString("name");
            toReturn = new Order(id, quantity, price, name);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }


    public static int insertOrder(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        ResultSet rs=null;
        int insertedId = 0;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            rs=insertStatement.getGeneratedKeys();
            if(rs.next()) {
                insertedId = rs.getInt(1);
            }
                insertStatement.setLong(1,insertedId);
                insertStatement.setInt(2, order.getQuantityOrder());
                insertStatement.setInt(3, order.getPrice());
                insertStatement.setString(4, order.getName());
                insertStatement.executeUpdate();


        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
        } finally {
            try{
            if(rs != null) rs.close();
            } catch(Exception ex){}
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
    public static void deleteOrder(String id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        Statement s = null;
        try {
            s = dbConnection.createStatement();

            String sql = "DELETE FROM order  WHERE " +
                    "id = '" + id + "' ";
            s.execute(sql);

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(dbConnection);
        }
    }
    public static void updateOrder(Order order) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStmt=null;

        int updatedId = -1;
        try {
            String q="UPDATE order SET quantity=?,price=? WHERE id=?";
            preparedStmt = dbConnection.prepareStatement(q);
            preparedStmt.setInt(1, order.getQuantityOrder());
            preparedStmt.setInt(2,  order.getPrice());
            preparedStmt.setLong(3, order.getId());
            preparedStmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "OrderDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(preparedStmt);
            ConnectionFactory.close(dbConnection);
        }
        //return updatedId;

    }
}
