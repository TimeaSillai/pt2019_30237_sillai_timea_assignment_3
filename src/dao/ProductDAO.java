package dao;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Client;
import model.Product;

public class ProductDAO {

    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO product (idProduct,quantityProduct,priceProduct,nameProduct)"
            + " VALUES (?,?,?,?)";
    private final static String findStatementString = "SELECT * FROM product where idProduct = ?";
    public static Product findByIdProduct(int id) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, id);
            rs = findStatement.executeQuery();
            rs.next();

            int quantity = rs.getInt("quantityProduct");
            int price = rs.getInt("priceProduct");
            String name =rs.getString("nameProduct");

            toReturn = new Product(id, quantity, price,name);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insertProduct(Product product) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        ResultSet rs=null;
        int insertedId = 0;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            rs=insertStatement.getGeneratedKeys();
            if(rs.next()) {
                insertedId = rs.getInt(1);
            }

            insertStatement.setLong(1,insertedId);
            insertStatement.setInt(2, product.getQuantityProduct());
            insertStatement.setInt(3, product.getPriceProduct());
            insertStatement.setString(4, product.getNameProduct());
            insertStatement.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
        } finally {
            try{
                if(rs != null) rs.close();
            } catch(Exception ex){}
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
    public static void deleteProduct(String id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        Statement s = null;
        try {
            s = dbConnection.createStatement();

            String sql = "DELETE FROM product  WHERE " +
                    "idProduct = '" + id + "' ";
            s.execute(sql);

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
        } finally {
            ConnectionFactory.close(dbConnection);
        }
    }
    public static void updateProduct(Product product, String id) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStmt=null;

        int updatedId = -1;
        try {
            String q="UPDATE product SET quantityProduct=?,priceProduct=?, nameProduct=? WHERE " + "idProduct= '" + id + "' ";
            preparedStmt = dbConnection.prepareStatement(q);
            //preparedStmt.setLong(1, product.getIdProduct());
            preparedStmt.setInt(1,product.getQuantityProduct() );
            preparedStmt.setInt(2, product.getPriceProduct() );
            preparedStmt.setString(3,product.getNameProduct());
            preparedStmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(preparedStmt);
            ConnectionFactory.close(dbConnection);
        }
        //return updatedId;



    }
    public static void updateQ(int q, String id){
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement preparedStmt=null;

        int updatedId = -1;
        try {
            String que="UPDATE product SET quantityProduct=? WHERE " + "idProduct= '" + id + "' ";
            preparedStmt = dbConnection.prepareStatement(que);
            //preparedStmt.setLong(1, product.getIdProduct());
            preparedStmt.setInt(1,q);
            preparedStmt.executeUpdate();

        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:update " + e.getMessage());
        } finally {
            ConnectionFactory.close(preparedStmt);
            ConnectionFactory.close(dbConnection);
        }
        //return updatedId;

    }
}