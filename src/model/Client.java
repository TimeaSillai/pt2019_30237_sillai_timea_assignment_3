package model;

public class Client {
	
	private int idClient;
	private String nameClient;
	private String addressClient;
	private String email;
	
	public Client(int idClient, String nameClient, String addressClient, String email) {
		super();
		this.idClient = idClient;
		this.nameClient = nameClient;
		this.addressClient = addressClient;
		this.email=email;
	}
	public Client(String nameClient, String addressClient,String email) {
		super();
		this.nameClient = nameClient;
		this.addressClient = addressClient;
		this.email=email;
	}
	
	public int getIdClient() {
		
		return idClient;
	}
	public void setIdClient(int idClient) {
		this.idClient = idClient;
	}
	
	public String getNameClient() {
		return nameClient;
	}
	
	public void setNameClient(String nameClient) {
		this.nameClient = nameClient;
	}
	
	public String getAddressClient() {
		return addressClient;
	}
	public void seClientAddress(String addressClient) {
		this.addressClient = addressClient;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
				return "Student [id=" + idClient + ", name=" + nameClient + ", address=" + addressClient + "]";
			}
}