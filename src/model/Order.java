package model;

public class Order {
private int id;
private int quantityOrder ,price;
private String name;
	public Order(int quantityOrder, int price, String name) {
		super();
		this.quantityOrder = quantityOrder;
		this.price = price;
		this.name=name;
	}

	public Order(int id, int quantityOrder, int price, String name) {
		super();
		this.id = id;
		this.quantityOrder = quantityOrder;
		this.price = price;
		this.name=name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantityOrder() {
		return quantityOrder;
	}

	public void setQuantityOrder(int quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getName() { return name; }

	public void setName(String name) { this.name = name; }

	@Override
	public String toString(){
		return "Order [id= " + id+ ", quantity=  " + quantityOrder +
				",  price= " + price +  ", name = " + name + "]";
	}
}
