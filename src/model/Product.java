package model;

public class Product {

	private int idProduct;
	private int quantityProduct,priceProduct;
	private String nameProduct;
	public Product(int idProduct, int quantityProduct, int priceProduct,String nameProduct)
	{
		super();
		this.idProduct=idProduct;
		this.nameProduct = nameProduct;
		this.priceProduct = priceProduct;
		this.quantityProduct = quantityProduct;
	}

	public Product(int quantityProduct, int priceProduct, String nameProduct) {
		super();
		this.quantityProduct = quantityProduct;
		this.priceProduct = priceProduct;
		this.nameProduct = nameProduct;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}
	
	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}
	
	public int getPriceProduct() {
		return priceProduct;
		}

	public void setProductPrice(int priceProduct) {
			this.priceProduct = priceProduct;
		}
		
	
	public int getQuantityProduct() {
		return quantityProduct;
	}

	public void setQuantityProduct(int quantityProduct) {
		this.quantityProduct = quantityProduct;
	}

	@Override
	public String toString() {
		return "Product [id=" + idProduct + ", quantity=" + quantityProduct + ", price=" + priceProduct + ", name= "+ nameProduct +  "]";
	}
}