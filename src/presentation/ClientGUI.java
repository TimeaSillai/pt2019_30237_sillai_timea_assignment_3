package presentation;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import bll.ClientBLL;
import model.Client;
import start.Start;
import connection.ConnectionFactory;

public class ClientGUI extends JFrame {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
	static JTable table;


	/**
	 * Create the frame.
	 */
	public ClientGUI() {

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(600, 500, 750, 750);
		setTitle("WAREHOUSE");
		getContentPane().setLayout(null);



		JLabel numeClientL= new JLabel ("Nume:");
		JTextField numeClientTX = new JTextField(10);
		JLabel adresaClientL= new JLabel ("Adresa:");
		JTextField adresaClientTX= new JTextField(10);
		JLabel emailClientL= new JLabel ("Email:");
		JTextField emailClientTX= new JTextField(10);

		numeClientL.setBounds(50, 250, 87, 89);
		getContentPane().add(numeClientL);
		adresaClientL.setBounds(150, 250, 87, 89);
		getContentPane().add(adresaClientL);
		emailClientL.setBounds(250,250 ,87, 89);
		getContentPane().add(emailClientL);


		numeClientTX.setBounds(50, 300, 100, 30);
		getContentPane().add(numeClientTX);
		adresaClientTX.setBounds(150, 300, 100, 30);
		getContentPane().add(adresaClientTX);
		emailClientTX.setBounds(250, 300, 100, 30);
		getContentPane().add(emailClientTX);


		// Client List
		JLabel lblClientList = new JLabel("Client List");
		lblClientList.setBounds(207, 44, 87, 14);
		getContentPane().add(lblClientList);

		// ScrollPane
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 84, 440, 89);
		getContentPane().add(scrollPane);

		// Table
		table = new JTable();
		scrollPane.setViewportView(table);

		// Button Delete
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				String ClientID = table.getValueAt(index, 0).toString();
				DeleteData(ClientID); // Delete Data
				PopulateData(); // Reload Table

			}
		});
		btnDelete.setBounds(100, 203, 89, 23);
		getContentPane().add(btnDelete);

		PopulateData();
		JButton btnInsert = new JButton("Insert");
		btnInsert.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String clientNameGot = numeClientTX.getText().toString();
				String clientAddressGot = adresaClientTX.getText().toString();
				String emailAddressGot = emailClientTX.getText().toString();
				//int clientIdDGot = Integer.parseInt(idClientTX.getText().toString());
				Client client = new Client(clientNameGot, clientAddressGot, emailAddressGot);
				InsertData(client);
				PopulateData();
			}
		});
		btnInsert.setBounds(200, 203, 89, 23);
		getContentPane().add(btnInsert);


		//button update
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = table.getSelectedRow();
				String ClientID = table.getValueAt(index, 0).toString();
				String clientNameGot = numeClientTX.getText().toString();
				String clientAddressGot = adresaClientTX.getText().toString();
				String clientEmailGot= emailClientTX.getText().toString();
				//int clientIdDGot = ;
				Client client = new Client(clientNameGot, clientAddressGot, clientEmailGot);
				UpdateData(client,ClientID);
				PopulateData();

			}
		});
		btnUpdate.setBounds(300, 203, 89, 23);
		getContentPane().add(btnUpdate);


	}

	private static void PopulateData() {

		// Clear table
		table.setModel(new DefaultTableModel());

		// Model for Table
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.addColumn("idClient");
		model.addColumn("nameClient");
		model.addColumn("addressClient");
		model.addColumn("email");


		Connection dbConnection = ConnectionFactory.getConnection();
		Statement s = null;
		try {
			s = dbConnection.createStatement();

			String sql = "SELECT * FROM  client";
			ResultSet rec = s.executeQuery(sql);
			int row = 0;
			while ((rec != null) && (rec.next())) {
				model.addRow(new Object[0]);
				model.setValueAt(rec.getString("idClient"), row, 0);
				model.setValueAt(rec.getString("nameClient"), row, 1);
				model.setValueAt(rec.getString("addressClient"), row, 2);
				model.setValueAt(rec.getString("email"), row, 3);
				row++;
			}

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}
	}

	private void DeleteData(String id) {
		ClientBLL clientbll= new ClientBLL();

		try {

			clientbll.deleteClient(id);
			JOptionPane.showMessageDialog(null, "Delete Data Successfully");

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}

	}

	private void InsertData(Client client) {
		ClientBLL clientbll= new ClientBLL();

		try {

			clientbll.insertClient(client);
			JOptionPane.showMessageDialog(null, "Insert Data Successfully");

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}
	}

	private void UpdateData(Client client,String id) {
		ClientBLL clientbll= new ClientBLL();


		try {
			clientbll.updateClient(client, id);
			JOptionPane.showMessageDialog(null, "Update Data Successfully");

		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
			e.printStackTrace();
		}

	}


}