package presentation;


import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.logging.Logger;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

import bll.ProductBLL;
import model.Product;
import start.Start;
import connection.ConnectionFactory;

public class ProductGUI extends JFrame {
    protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
    static JTable table;


    /**
     * Create the frame.
     */
    public ProductGUI() {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(750, 750, 750, 750);
        setTitle("WAREHOUSE");
        getContentPane().setLayout(null);


        JLabel nume= new JLabel ("Nume:");
        JTextField numeTF = new JTextField(10);
        JLabel pret= new JLabel ("Pret:");
        JTextField pretTF= new JTextField(10);
        JLabel cantitate= new JLabel ("Cantitate:");
        JTextField cantitateTF= new JTextField(10);

        nume.setBounds(50, 250, 87, 89);
        getContentPane().add(nume);
        pret.setBounds(150, 250, 87, 89);
        getContentPane().add(pret);
        cantitate.setBounds(250,250 ,87, 89);
        getContentPane().add(cantitate);

        numeTF.setBounds(50, 300, 100, 30);
        getContentPane().add(numeTF);
        pretTF.setBounds(150, 300, 100, 30);
        getContentPane().add(pretTF);
        cantitateTF.setBounds(250, 300, 100, 30);
        getContentPane().add(cantitateTF);


        // Product List
        JLabel lblProductList = new JLabel("Product List");
        lblProductList.setBounds(207, 44, 87, 14);
        getContentPane().add(lblProductList);

        // ScrollPane
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setBounds(28, 84, 440, 89);
        getContentPane().add(scrollPane);

        // Table
        table = new JTable();
        scrollPane.setViewportView(table);

        // Button Delete
        JButton btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = table.getSelectedRow();
                String ID = table.getValueAt(index, 0).toString();
                DeleteData(ID); // Delete Data
                PopulateData(); // Reload Table

            }
        });
        btnDelete.setBounds(100, 203, 89, 23);
        getContentPane().add(btnDelete);

        PopulateData();
        JButton btnInsert = new JButton("Insert");
        btnInsert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String nameGot = numeTF.getText().toString();
                int  pretGot = Integer.parseInt(pretTF.getText().toString());
                int quantityGot = Integer.parseInt(cantitateTF.getText().toString());
                //int IdDGot = Integer.parseInt(idTF.getText().toString());
                Product product = new Product(quantityGot, pretGot,nameGot);
                InsertData(product);
                PopulateData();
            }
        });
        btnInsert.setBounds(200, 203, 89, 23);
        getContentPane().add(btnInsert);


        //button update
        JButton btnUpdate = new JButton("Update");
        btnUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int index = table.getSelectedRow();
                String ID = table.getValueAt(index, 0).toString();
                String nameGot = numeTF.getText().toString();
                int priceGot = Integer.parseInt(pretTF.getText().toString());
                int quantityGot= Integer.parseInt(cantitateTF.getText().toString());
                //int clientIdDGot = ;
                Product product = new Product(quantityGot,priceGot,nameGot);
                UpdateData(product,ID);
                PopulateData();

            }
        });
        btnUpdate.setBounds(300, 203, 89, 23);
        getContentPane().add(btnUpdate);


    }

    private static void PopulateData() {

        // Clear table
        table.setModel(new DefaultTableModel());

        // Model for Table
        DefaultTableModel model = (DefaultTableModel) table.getModel();
        model.addColumn("idProduct");
        model.addColumn("quantityProduct");
        model.addColumn("priceProduct");
        model.addColumn("nameProduct");


        Connection dbConnection = ConnectionFactory.getConnection();
        Statement s = null;
        try {
            s = dbConnection.createStatement();

            String sql = "SELECT * FROM  product";
            ResultSet rec = s.executeQuery(sql);
            int row = 0;
            while ((rec != null) && (rec.next())) {
                model.addRow(new Object[0]);
                model.setValueAt(rec.getString("idProduct"), row, 0);
                model.setValueAt(rec.getString("quantityProduct"), row, 1);
                model.setValueAt(rec.getString("priceProduct"), row, 2);
                model.setValueAt(rec.getString("nameProduct"), row, 3);
                row++;
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }
    }

    private void DeleteData(String id) {
        ProductBLL productBLL= new ProductBLL();

        try {

            productBLL.deleteProduct(id);
            JOptionPane.showMessageDialog(null, "Delete Data Successfully");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }

    }

    private void InsertData(Product product) {
        ProductBLL productBLL= new ProductBLL();

        try {

            productBLL.insertProduct(product);
            JOptionPane.showMessageDialog(null, "Insert Data Successfully");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }
    }

    private void UpdateData(Product product,String id) {
        ProductBLL productBLL= new ProductBLL();


        try {
            productBLL.updateProduct(product,id);
            JOptionPane.showMessageDialog(null, "Update Data Successfully");

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage());
            e.printStackTrace();
        }

    }


}