package presentation;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
	import java.sql.ResultSet;
	import java.sql.SQLException;
	import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
	import javax.swing.JOptionPane;
import javax.swing.JPanel;

import javax.swing.JTable;
import javax.swing.JTextField;

import javax.swing.JScrollPane;
	import javax.swing.table.DefaultTableModel;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;
import start.ReflectionExample;
import start.Start;
import connection.ConnectionFactory;
import javax.swing.SwingUtilities;
import javax.swing.JLabel;


import javax.swing.JButton;
	import java.awt.event.ActionListener;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

	public class View extends JFrame {
		protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
		static JTable table;
		static JTable table2;
		 public static void main(String[] args) {
		        SwingUtilities.invokeLater(new Runnable() {
		            public void run() {
		                createAndShowGUI();
		            }
		        });
		    }
				
		public static void createAndShowGUI() {

			JFrame frame = new JFrame("Client");
			frame.setSize(750,750);
			frame.setVisible(true);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setTitle("WAREHOUSE");
			frame.getContentPane().setLayout(null);
			JPanel panelClient = new JPanel();
			JPanel panel = new JPanel();


			// Client List
			JLabel lblClientList = new JLabel("Client List");
			lblClientList.setBounds(207, 44, 87, 14);
			frame.getContentPane().add(lblClientList);

			// ScrollPane
			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(28, 84, 440, 89);
			frame.getContentPane().add(scrollPane);

			// Table
			table = new JTable();
			scrollPane.setViewportView(table);




			//-----------------------------------------------------------------------------ProductList


			// Product List
			JLabel lblProductList = new JLabel("Product List");
			lblProductList.setBounds(207, 200, 87, 14);
			frame.getContentPane().add(lblProductList);

			// ScrollPane
			JScrollPane scrollPane2 = new JScrollPane();
			scrollPane2.setBounds(28, 250, 440, 89);
			frame.getContentPane().add(scrollPane2);

			// Table
			table2 = new JTable();
			scrollPane2.setViewportView(table2);


			PopulateData();
			PopulateDataProduct();


			JLabel cantitate= new JLabel ("Cantitate:");
			JTextField cantitateTF= new JTextField(10);
			cantitate.setBounds(500,150 ,87, 89);
			frame.getContentPane().add(cantitate);
			cantitateTF.setBounds(500, 200, 100, 30);
			frame.getContentPane().add(cantitateTF);

			//Select
			JButton btnSelect= new JButton("Order");
			btnSelect.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {

					int index = table.getSelectedRow();
					String ClientID = table.getValueAt(index, 0).toString();


					int index2 = table2.getSelectedRow();
					String ClientID2 = table2.getValueAt(index2, 0).toString();
					int quantityGot= Integer.parseInt(cantitateTF.getText().toString());
					selectOrder(ClientID,ClientID2, quantityGot);
					InsertData(ClientID,ClientID2, quantityGot);
					PopulateData();
					PopulateDataProduct();


				}
			});

			btnSelect.setBounds(500, 280, 89, 23);
			frame.getContentPane().add(btnSelect);


			//Button for product
			JButton productB = new JButton("Product");
			productB.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
				ProductGUI productGUI = new ProductGUI();
				productGUI.setVisible(true);

				}
			});
			productB.setBounds(100, 400, 89, 23);
			frame.getContentPane().add(productB);

			//Button for clients
			JButton clientB = new JButton("Client");
			clientB.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					ClientGUI clientGUI = new ClientGUI();
					clientGUI.setVisible(true);

				}
			});
			clientB.setBounds(200, 400, 89, 23);
			frame.getContentPane().add(clientB);

		}

		private static void PopulateDataProduct() {

			// Clear table
			table2.setModel(new DefaultTableModel());

			// Model for Table
			DefaultTableModel model = (DefaultTableModel) table2.getModel();
			model.addColumn("idProduct");
			model.addColumn("quantityProduct");
			model.addColumn("priceProduct");
			model.addColumn("nameProduct");


			Connection dbConnection = ConnectionFactory.getConnection();
			Statement s = null;
			try {
				s = dbConnection.createStatement();

				String sql = "SELECT * FROM  product";
				ResultSet rec = s.executeQuery(sql);
				int row = 0;
				while ((rec != null) && (rec.next())) {
					model.addRow(new Object[0]);
					model.setValueAt(rec.getString("idProduct"), row, 0);
					model.setValueAt(rec.getString("quantityProduct"), row, 1);
					model.setValueAt(rec.getString("priceProduct"), row, 2);
					model.setValueAt(rec.getString("nameProduct"), row, 3);
					row++;
				}

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
		}

		private static void PopulateData() {

			// Clear table
			table.setModel(new DefaultTableModel());

			// Model for Table
			DefaultTableModel model = (DefaultTableModel) table.getModel();
			model.addColumn("idClient");
			model.addColumn("nameClient");
			model.addColumn("addressClient");
			model.addColumn("email");


			Connection dbConnection = ConnectionFactory.getConnection();
			Statement s = null;
			try {
				s = dbConnection.createStatement();

				String sql = "SELECT * FROM  client";
				ResultSet rec = s.executeQuery(sql);
				int row = 0;
				while ((rec != null) && (rec.next())) {
					model.addRow(new Object[0]);
					model.setValueAt(rec.getString("idClient"), row, 0);
					model.setValueAt(rec.getString("nameClient"), row, 1);
					model.setValueAt(rec.getString("addressClient"), row, 2);
					model.setValueAt(rec.getString("email"), row, 3);
					row++;
				}

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
		}

		private static void selectOrder(String idClient, String idProdus, int cantitate){
			ProductBLL productBLL = new ProductBLL();

			Product pro= productBLL.findProductById(Integer.parseInt(idProdus));
			if(cantitate < pro.getQuantityProduct()){
				try{
					productBLL.updateQ(pro.getQuantityProduct()-cantitate, idProdus);
					JOptionPane.showMessageDialog(null, "Update Data Successfully");
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, "Under stock!!");
					e.printStackTrace();
				}
			}

		}

		private static void InsertData(String idClient,String idProdus, int cantitate) {
			OrderBLL orderBLL = new OrderBLL();
			ProductBLL productBLL = new ProductBLL();
			ClientBLL clientBLL = new ClientBLL();
			Client client = clientBLL.findClientById(Integer.parseInt(idClient));
			Product product= productBLL.findProductById(Integer.parseInt(idProdus));
			Order order = new Order(cantitate,product.getPriceProduct(), client.getNameClient());
			try {

				orderBLL.insertOrder(order);
				Bill(order);
				JOptionPane.showMessageDialog(null, "Insert Data Successfully");

			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, e.getMessage());
				e.printStackTrace();
			}
			if(cantitate == product.getQuantityProduct()){
				try{
					productBLL.deleteProduct(idProdus);
					Bill(order);
					JOptionPane.showMessageDialog(null, "Delete Data Successfully");
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null, e.getMessage());
					e.printStackTrace();
				}
			}
		}

		private static void Bill(Order order){
		 	try{
				File file = new File("C:\\Mind field\\TP\\PT2018_30227_Sillai_Timea_Assignment_3/Bill.txt");
				if (!file.exists())
				{
					file.createNewFile();
				}


				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				Date today = new Date();
				SimpleDateFormat formatter = new SimpleDateFormat("dd MM yyyy");
				SimpleDateFormat formatter1 = new SimpleDateFormat("h:mm a");
				bw.write("WAREHOUSE");
				bw.newLine();
				bw.write("Date: " + formatter.format(today));
				bw.newLine();
				bw.write("Time: " + formatter1.format(today));
				bw.newLine();
				bw.newLine();
				bw.write("Id: " + order.getId() );
				bw.newLine();
				bw.write("Quantity: " + order.getQuantityOrder());
				bw.newLine();
				bw.write("Price: " + order.getPrice());
				bw.newLine();
				bw.write("Name: " + order.getName() );
				bw.newLine();
				bw.newLine();

				bw.close();

			} catch (IOException e) {
		 		e.printStackTrace();
		 	}


		}

	}

