package start;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import bll.ClientBLL;
import bll.OrderBLL;
import model.Client;
import model.Order;


public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());

	public static void main(String[] args) throws SQLException {

		//Client client = new Client(2,"ummy","dummy", "dummy@gmail.com");

		//ClientBLL clientbll= new ClientBLL();
		//clientbll.insertClient(client);

		//clientbll.updateClient(client,"3");
		Order order = new Order(3,3,"dummy");
		OrderBLL orderBLL=new OrderBLL();
		orderBLL.insertOrder(order);
		try {
		//	clientbll.findClientById(2);
		} catch (Exception ex) {
			LOGGER.log(Level.INFO, ex.getMessage());
		}
		
		
		//obtain field-value pairs for object through reflection
		ReflectionExample.retrieveProperties(order);
	}
	
	

}
